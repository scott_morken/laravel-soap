<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/21/17
 * Time: 9:54 AM
 */

namespace Smorken\Soap;

use Illuminate\Contracts\Config\Repository;
use Phpro\SoapClient\Soap\ClassMap\ClassMapCollection;
use Smorken\Soap\Contracts\Soap\Client;
use Smorken\Soap\Parsers\StdClassToArray;

class Model extends \Smorken\Model\VO\Model implements \Smorken\Soap\Contracts\Soap\Model
{

    protected static $clientClass;

    /**
     * @var Repository
     */
    protected $config;

    /**
     * @var string
     */
    protected $request_class = Request::class;

    /**
     * @var string
     */
    protected $wsdl;
    /**
     * @var mixed
     */
    protected $classMaps;
    /**
     * class name of provider
     * @var string
     */
    protected $classMapProvider;
    /**
     * @var array
     */
    protected $parser_class = [StdClassToArray::class];
    /**
     * @var string|null
     */
    protected $result_key;
    /**
     * @var array
     */
    protected $last_soap_request;

    protected $client_options = [];

    protected $soap_options = [];

    protected $elapsed = [];

    /**
     * @param $parser_class
     */
    public function addParserClass($parser_class)
    {
        $this->parser_class[] = $parser_class;
    }

    public function setParserClass($parser_class)
    {
        $this->parser_class = [$parser_class];
    }

    /**
     * @return array
     */
    public function getParsers()
    {
        $parsers = [];
        if ($this->parser_class) {
            foreach ($this->parser_class as $parser_class) {
                $parsers[] = new $parser_class;
            }
        }
        return $parsers;
    }

    /**
     * The array key that will bring back the result set for this model
     * @return null|string
     */
    public function getResultKey()
    {
        return $this->result_key;
    }

    /**
     * The array key that will bring back the result set for this model
     * @param $key
     */
    public function setResultKey($key)
    {
        $this->result_key = $key;
    }

    /**
     * @return array|null
     */
    public function getLastSoapRequest()
    {
        return $this->last_soap_request;
    }

    /**
     * @return array|null
     */
    public function getLastRequest()
    {
        return $this->getLastSoapRequest();
    }

    /**
     * @param $data
     */
    public function setLastSoapRequest($data)
    {
        $this->last_soap_request = $data;
    }

    /**
     * Get a new request builder for the model.
     *
     * @return \Smorken\Soap\Contracts\Soap\Request
     */
    public function newRequest()
    {
        $client_class = $this->getClientClass();
        $builder = $this->getRequest($client_class);
        $b = $builder->setModel($this)
                     ->wsdl($this->getWsdl())
                     ->soapOptions($this->getSoapOptions());
        if ($this->getHttpHeaders()) {
            $b->httpHeaders($this->getHttpHeaders());
        }
        if ($this->getClassMaps()) {
            $b->classMap($this->getClassMaps());
        }
        return $b;
    }

    protected function getRequest($client_class)
    {
        return new $this->{'request_class'}($client_class);
    }

    /**
     * Get the client for the model.
     *
     * @return Client
     */
    public function getClientClass()
    {
        if (!static::$clientClass) {
            $cls = \Smorken\Soap\Client::class;
            static::setClientClass($cls);
        }
        return static::$clientClass;
    }

    /**
     * Set the client factory
     *
     * @param $client_class
     */
    public static function setClientClass($client_class)
    {
        static::$clientClass = $client_class;
    }

    /**
     * @return string
     */
    public function getWsdl()
    {
        return $this->wsdl;
    }

    /**
     * @param string $wsdl
     */
    public function setWsdl($wsdl)
    {
        $this->wsdl = $wsdl;
    }

    public function getSoapOptions()
    {
        if (!$this->soap_options) {
            $this->setSoapOptions($this->getConfig()->get('soap.soap_options', []));
        }
        return $this->soap_options;
    }

    public function setSoapOptions($options)
    {
        $this->soap_options = $options;
    }

    protected function getHttpHeaders()
    {
        $opts = $this->getClientOptions();
        if (isset($opts['headers'])) {
            return $opts['headers'];
        }
    }

    public function getClientOptions()
    {
        if (!$this->client_options) {
            $this->setClientOptions($this->getConfig()->get('soap.client_options', []));
        }
        return $this->client_options;
    }

    public function setClientOptions($options)
    {
        $this->client_options = $options;
    }

    /**
     * @return Repository
     */
    protected function getConfig()
    {
        if (!$this->config) {
            $this->config = app(Repository::class);
        }
        return $this->config;
    }

    /**
     * @param Repository $config
     */
    public function setConfig(Repository $config)
    {
        $this->config = $config;
    }

    /**
     * @return mixed
     */
    public function getClassMaps()
    {
        if (!$this->classMaps && $this->classMapProvider) {
            $this->classMaps = call_user_func([$this->classMapProvider, 'get']);
        }
        return $this->classMaps;
    }

    /**
     * @param mixed $classMaps
     */
    public function setClassMaps($classMaps)
    {
        $this->classMaps = $classMaps;
    }

    /**
     * @param string $key
     * @return int
     */
    public function getElapsed($key = 'total')
    {
        return (float)(array_get($this->elapsed, $key, 0) - array_get($this->elapsed, 'start', 0));
    }

    /**
     * @param array $elapsed
     */
    public function setElapsed(array $elapsed)
    {
        $this->elapsed = $elapsed;
    }
}
