<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/29/17
 * Time: 9:09 AM
 */

namespace Smorken\Soap\Zend;

class Request extends \Smorken\Soap\Request implements \Smorken\Soap\Contracts\Soap\Request
{

    protected function buildClient()
    {
        $cls = $this->clientClass;
        $soap_opts = $this->getSoapOptions();
        unset($soap_opts['trace']);
        $this->addHttpHeaders($soap_opts);
        $this->addClassMap($soap_opts);
        $client = new $cls($this->getWsdl(), $soap_opts);
        $this->addSoapHeaders($client);
        $this->addModifiers($client);
        return $client;
    }
}
