<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/29/17
 * Time: 9:08 AM
 */

namespace Smorken\Soap\Zend;

use Zend\Soap\Client\Common;

class Client extends \Zend\Soap\Client implements \Smorken\Soap\Contracts\Soap\Client
{

    use \Smorken\Soap\Traits\Client;

    // @codingStandardsIgnoreStart

    /**
     * Do request proxy method.
     *
     * May be overridden in subclasses
     *
     * @param  Common $client
     * @param  string $request
     * @param  string $location
     * @param  string $action
     * @param  int $version
     * @param  int $oneWay
     * @return mixed
     */
    public function _doRequest(Common $client, $request, $location, $action, $version, $oneWay = null)
    {
        $request = $this->modify('request', $request);
        $location = $this->modify('location', $location);
        $action = $this->modify('action', $action);
        $version = $this->modify('version', $version);
        return parent::_doRequest($client, $request, $location, $action, $version, $oneWay);
    }

    // @codingStandardsIgnoreEnd

    public function debugLastSoapRequest()
    {
        return [
            'request'  => [
                'headers' => $this->getLastRequestHeaders(),
                'body'    => $this->getLastRequest(),
            ],
            'response' => [
                'headers' => $this->getLastResponseHeaders(),
                'body'    => $this->getLastResponse(),
            ],
        ];
    }

    /**
     * @param \SoapHeader|\SoapHeader[] $soapHeaders
     * @return mixed
     */
    public function applySoapHeaders($soapHeaders)
    {
        if (is_array($soapHeaders)) {
            foreach ($soapHeaders as $header) {
                $this->addSoapInputHeader($header);
            }
        } else {
            $this->addSoapInputHeader($soapHeaders);
        }
    }
}
