<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/29/17
 * Time: 9:08 AM
 */

namespace Smorken\Soap\Zend;

class Model extends \Smorken\Soap\Model implements \Smorken\Soap\Contracts\Soap\Model
{

    protected static $clientClass;

    protected $request_class = Request::class;

    /**
     * Get the client for the model.
     *
     * @return Client
     */
    public function getClientClass()
    {
        if (!static::$clientClass) {
            $cls = Client::class;
            static::setClientClass($cls);
        }
        return static::$clientClass;
    }
}
