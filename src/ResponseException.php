<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 2:58 PM
 */

namespace Smorken\Soap;

class ResponseException extends \Exception implements \Smorken\Model\Contracts\ResponseException
{

}
