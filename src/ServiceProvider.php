<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 3:25 PM
 */

namespace Smorken\Soap;

use Phpro\SoapClient\ClientFactory;
use Phpro\SoapClient\ClientFactoryInterface;
use Smorken\Soap\Contracts\Type\ArrayToXml;
use Smorken\Soap\Contracts\Type\XmlToArray;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function boot()
    {
        $this->bootConfig();
        \Smorken\Soap\Model::setClientClass(\Smorken\Soap\Client::class);
    }

    protected function bootConfig()
    {
        $config = __DIR__ . '/../config/config.php';
        $this->mergeConfigFrom($config, 'soap');
        $this->publishes([$config => config_path('soap.php')], 'config');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            ClientFactoryInterface::class,
            function ($app) {
                return new ClientFactory(\Smorken\Soap\Client::class);
            }
        );
        $this->app->bind(
            XmlToArray::class,
            function ($app) {
                return new \Smorken\Soap\Type\XmlToArray();
            }
        );
        $this->app->bind(
            ArrayToXml::class,
            function ($app) {
                return new \Smorken\Soap\Type\ArrayToXml();
            }
        );
    }
}
