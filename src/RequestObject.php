<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/28/17
 * Time: 1:12 PM
 */

namespace Smorken\Soap;

use Smorken\Soap\Type\ArrayToXml;
use Smorken\Soap\Type\Base;

class RequestObject implements \Smorken\Soap\Contracts\Soap\RequestObject
{

    protected $name;

    protected $request;

    /**
     * @var \Smorken\Soap\Contracts\Type\ArrayToXml
     */
    protected $xml_converter;

    public function __construct($request, $name)
    {
        $this->request = $request;
        $this->name = $name;
    }

    public function getRequest($xml_first = false)
    {
        if ($xml_first) {
            $this->convertToXml();
        }
        $type = is_string($this->request) ? XSD_ANYXML : SOAP_ENC_OBJECT;
        return new \SoapVar($this->request, $type, null, null, $this->getName());
    }

    public function getName()
    {
        if (!$this->name) {
            throw new RequestException("Name must be set");
        }
        return $this->name;
    }

    protected function convertToXml()
    {
        $cx = $this->getConverter();
        if (is_array($this->request)) {
            $this->request = $cx->create($this->request, $this->getName());
        } elseif ($this->request instanceof Base) {
            $this->request = $cx->create($this->request->toArray(), $this->getName());
        }
    }

    protected function getConverter()
    {
        if (!$this->xml_converter) {
            $this->xml_converter = new ArrayToXml();
        }
        return $this->xml_converter;
    }

    public function setXmlConverter(\Smorken\Soap\Contracts\Type\ArrayToXml $converter)
    {
        $this->xml_converter = $converter;
    }
}
