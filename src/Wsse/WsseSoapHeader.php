<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/21/17
 * Time: 7:47 AM
 */

namespace Smorken\Soap\Wsse;

class WsseSoapHeader extends \SoapHeader
{

    protected $ns = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';

    public function __construct($username, $password = null, $ns = null)
    {
        if ($ns) {
            $this->ns = $ns;
        }
        $auth = $this->createAuth($username, $password);
        $token = $this->getUsernameToken($auth);
        $security = $this->createSecurityVar($token);
        parent::__construct($this->ns, 'Security', $security, true);
    }

    protected function createAuth($username, $password)
    {
        $auth = new \stdClass();
        $auth->Username = new \SoapVar($username, XSD_STRING, null, $this->ns, null, $this->ns);
        if (!is_null($password)) {
            $auth->Password = new \SoapVar($password, XSD_STRING, null, $this->ns, null, $this->ns);
        }
        return $auth;
    }

    protected function getUsernameToken($auth)
    {
        $token = new \stdClass();
        $token->UsernameToken = new \SoapVar($auth, SOAP_ENC_OBJECT, null, $this->ns, 'UsernameToken', $this->ns);
        return $token;
    }

    protected function createSecurityVar($token)
    {
        $v = new \SoapVar($token, SOAP_ENC_OBJECT, null, $this->ns, 'UsernameToken', $this->ns);
        return new \SoapVar($v, SOAP_ENC_OBJECT, null, $this->ns, 'Security', $this->ns);
    }
}
