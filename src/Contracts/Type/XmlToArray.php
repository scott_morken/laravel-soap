<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/29/17
 * Time: 12:10 PM
 */

namespace Smorken\Soap\Contracts\Type;

interface XmlToArray
{

    /**
     * @param $xml
     * @return array
     */
    public function parse($xml);
}
