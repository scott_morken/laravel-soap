<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/29/17
 * Time: 12:09 PM
 */

namespace Smorken\Soap\Contracts\Type;

interface ArrayToXml
{

    /**
     * @param array $array
     * @param string $root
     * @return string
     */
    public function create($array, $root = 'root');
}
