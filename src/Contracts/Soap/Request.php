<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 2:56 PM
 */

namespace Smorken\Soap\Contracts\Soap;

use Illuminate\Contracts\Logging\Log;

interface Request extends \Smorken\Model\Contracts\Request
{

    /**
     * @return string
     */
    public function getClientClass();

    /**
     * @param string $function
     * @param mixed $request
     * @return $this
     */
    public function asFunction($function, $request = null);

    /**
     * @return string
     */
    public function getFunction();

    /**
     * @param mixed $request
     * @return $this
     */
    public function request($request);

    /**
     * @return mixed
     */
    public function getRequest();

    /**
     * @param $key
     * @param callable|array $callable
     */
    public function modifier($key, $callable);

    /**
     * @return array
     */
    public function getModifiers();

    /**
     * @param string $header
     */
    public function httpHeader($header);

    /**
     * @param array $headers
     */
    public function httpHeaders($headers = []);

    /**
     * @return array
     */
    public function getHttpHeaders();

    /**
     * @param \SoapHeader $header
     */
    public function soapHeader(\SoapHeader $header);

    /**
     * @return array
     */
    public function getSoapHeaders();

    /**
     * @param string $key
     * @param mixed $value
     */
    public function soapOption($key, $value);

    /**
     * @param array $options
     */
    public function soapOptions($options = []);

    /**
     * @param string $wsdl
     */
    public function wsdl($wsdl);

    /**
     * @return string
     */
    public function getWsdl();

    /**
     * @return array
     */
    public function getSoapOptions();

    /**
     * @param mixed $classMap
     * @return $this
     */
    public function classMap($classMap);

    /**
     * @return $this
     */
    public function useResponse();

    /**
     * @return bool
     */
    public function shouldUseResponse();

    /**
     * @param Log $logger
     * @return $this
     */
    public function setLogger(Log $logger);

    /**
     * @return mixed
     */
    public function getClassMap();

    /**
     * Store the last soap request debug info on the model
     * @return $this
     */
    public function saveLastSoapRequest();

    /**
     * Shorter version of saveLastSoapRequest
     * @return $this
     */
    public function saveLast();

    public function setXmlParser($parser);

    /**
     * @param string $result
     * @return $this
     */
    public function useLast($result);

    /**
     * @return mixed
     */
    public function getUseLast();
}
