<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/15/17
 * Time: 6:49 AM
 */

namespace Smorken\Soap\Contracts\Soap;

interface ClassMapper
{

    /**
     * @return mixed
     */
    public static function get();
}
