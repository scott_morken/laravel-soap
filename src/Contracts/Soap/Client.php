<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 5:17 PM
 */

namespace Smorken\Soap\Contracts\Soap;

use Phpro\SoapClient\Type\RequestInterface;

interface Client
{

    /**
     * @param $func
     * @param RequestInterface|array $request
     * @return mixed
     */
    public function request($func, $request);

    public function debugLastSoapRequest();

    /**
     * @param $key
     * @param array|callable $handler
     * @return mixed
     */
    public function modifier($key, $handler);

    /**
     * @param \SoapHeader|\SoapHeader[] $soapHeaders
     * @return mixed
     */
    public function applySoapHeaders($soapHeaders);
}
