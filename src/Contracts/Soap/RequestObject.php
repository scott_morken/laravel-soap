<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/28/17
 * Time: 1:11 PM
 */

namespace Smorken\Soap\Contracts\Soap;

interface RequestObject
{

    /**
     * @return string
     */
    public function getName();

    /**
     * @return mixed
     */
    public function getRequest();

    /**
     * @param \Smorken\Soap\Contracts\Type\ArrayToXml $converter
     */
    public function setXmlConverter(\Smorken\Soap\Contracts\Type\ArrayToXml $converter);
}
