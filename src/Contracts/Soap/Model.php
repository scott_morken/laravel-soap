<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 2:56 PM
 */

namespace Smorken\Soap\Contracts\Soap;

use Illuminate\Contracts\Config\Repository;
use Phpro\SoapClient\Soap\ClassMap\ClassMapCollection;

interface Model extends \Smorken\Model\Contracts\Model
{

    /**
     * @return string
     */
    public function getWsdl();

    /**
     * @param string $wsdl
     */
    public function setWsdl($wsdl);

    /**
     * @return mixed
     */
    public function getClassMaps();

    /**
     * @param mixed $classMaps
     */
    public function setClassMaps($classMaps);

    /**
     * @param $parser_class
     */
    public function addParserClass($parser_class);

    /**
     * @param $parser_class
     */
    public function setParserClass($parser_class);

    /**
     * @return array
     */
    public function getParsers();

    /**
     * The array key that will bring back the result set for this model
     * @return null|string
     */
    public function getResultKey();

    /**
     * The array key that will bring back the result set for this model
     * @param $key
     */
    public function setResultKey($key);

    /**
     * @param $data
     */
    public function setLastSoapRequest($data);

    /**
     * @return array|null
     */
    public function getLastSoapRequest();

    /**
     * @return array|null
     */
    public function getLastRequest();

    /**
     * @param string $key
     * @return int
     */
    public function getElapsed($key = 'total');

    /**
     * @param array $elapsed
     */
    public function setElapsed(array $elapsed);

    /**
     * Get a new request builder for the model.
     *
     * @return \Smorken\Soap\Contracts\Soap\Request
     */
    public function newRequest();

    /**
     * @return array
     */
    public function getSoapOptions();

    /**
     * @param array $options
     */
    public function setSoapOptions($options);

    /**
     * @return array
     */
    public function getClientOptions();

    /**
     * @param $options
     */
    public function setClientOptions($options);

    /**
     * @param string $client_class
     * @return
     */
    public static function setClientClass($client_class);

    /**
     * @return string
     */
    public function getClientClass();

    /**
     * @param Repository $config
     */
    public function setConfig(Repository $config);
}
