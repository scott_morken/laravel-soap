<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/19/17
 * Time: 8:34 AM
 */

namespace Smorken\Soap\Parsers;

use Smorken\Soap\Contracts\Soap\Parser;
use Smorken\Soap\Parsers\Traits\ShouldArray;

class LovWithAttributes implements Parser
{

    use ShouldArray;

    /**
     * @param $response
     * @return mixed
     */
    public function parse($response)
    {
        if ($this->shouldConvert($response)) {
            $response = $this->convert($response);
        }
        return $this->toArray($response);
    }

    protected function toArray($response)
    {
        if (!$response) {
            return [];
        }
        $values = $this->getValues($response);
        return $this->valuesToArray($values);
    }

    protected function valuesToArray($values)
    {
        if (isset($values['VALUE'])) {
            if (is_array($values['VALUE'])) {
                $vs = [];
                if (array_keys($values['VALUE']) === range(0, count($values['VALUE']) - 1)) {
                    foreach ($values['VALUE'] as $i => $v) {
                        $vs[$i] = $v;
                    }
                } else {
                    $vs[] = $values['VALUE'];
                }
                return $vs;
            }
        }
        return $values;
    }

    protected function getValues($response)
    {
        $values = [];
        if (!$response) {
            return [];
        }
        if (is_array($response)) {
            if (isset($response['VALUES'])) {
                return $response['VALUES'];
            } else {
                foreach ($response as $k => $v) {
                    if (is_array($v)) {
                        $values += $this->getValues($v);
                    }
                }
            }
        }
        return $values;
    }
}
