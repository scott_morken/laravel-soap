<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/21/17
 * Time: 11:33 AM
 */

namespace Smorken\Soap\Parsers;

use Smorken\Soap\Contracts\Soap\Parser;

class StdClassToArray implements Parser
{

    /**
     * @param $response
     * @return mixed
     */
    public function parse($response)
    {
        return json_decode(json_encode($response), true);
    }
}
