<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/29/17
 * Time: 1:15 PM
 */

namespace Smorken\Soap\Parsers\Traits;

use Smorken\Soap\Parsers\StdClassToArray;

trait ShouldArray
{

    protected function shouldConvert($results)
    {
        if (!$results) {
            return false;
        }
        if ($results instanceof \stdClass) {
            return true;
        }
        if (is_array($results)) {
            foreach ($results as $k => $v) {
                return $this->shouldConvert($v);
            }
        }
        return false;
    }

    protected function convert($results)
    {
        $c = new StdClassToArray();
        return $c->parse($results);
    }
}
