<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/27/17
 * Time: 8:16 AM
 */

namespace Smorken\Soap\Parsers;

use Smorken\Soap\Contracts\Soap\Parser;
use Smorken\Soap\Parsers\Traits\ShouldArray;

class QueryServiceResults implements Parser
{

    use ShouldArray;

    /**
     * @param $response
     * @return mixed
     */
    public function parse($response)
    {
        if ($this->shouldConvert($response)) {
            $response = $this->convert($response);
        }
        $rows = $this->getRows($response);
        return $this->rowsToArray($rows);
    }

    protected function getRows($response)
    {
        if (isset($response['row'])) {
            return $response['row'];
        }
        $rows = [];
        foreach ($response as $k => $v) {
            if (is_array($v)) {
                if (isset($v['row'])) {
                    $rows = $rows + $v['row'];
                } else {
                    $rows = $rows + $this->getRows($v);
                }
            }
        }
        return $rows;
    }

    protected function rowsToArray($rows)
    {
        $result = [];
        $rows = $this->verifyRowsIsArray($rows);
        foreach ($rows as $row) {
            if (isset($row['any'])) {
                $value = $row['any'];
                if (strpos($value, '/>') !== false) {
                    $value = '<?xml version=\'1.0\'?><body>' . $value . '</body>';
                    $value = $this->xmlToArray($value);
                }
                $result[] = $value;
            } else {
                $result[] = $row;
            }
        }
        return $result;
    }

    protected function verifyRowsIsArray($rows)
    {
        foreach ($rows as $id => $row) {
            if (!is_array($row)) {
                return [$rows];
            }
        }
        return $rows;
    }

    protected function xmlToArray($xml)
    {
        $xml_arr = (array)simplexml_load_string($xml);
        $new = [];
        foreach ($xml_arr as $k => $v) {
            $value = (string)$v;
            $new[$k] = $value;
        }
        $new = array_map('trim', $new);
        return $new;
    }
}
