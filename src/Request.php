<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/21/17
 * Time: 9:54 AM
 */

namespace Smorken\Soap;

use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\Collection;
use Smorken\Soap\Contracts\ResponseException;
use Smorken\Soap\Type\XmlToArray;

class Request implements \Smorken\Soap\Contracts\Soap\Request
{

    /**
     * @var string
     */
    protected $clientClass;

    /**
     * @var \Smorken\Soap\Contracts\Soap\Model
     */
    protected $model;

    protected $wsdl;

    protected $function;

    /**
     * @var mixed
     */
    protected $request;

    /**
     * @var mixed
     */
    protected $classMap;

    protected $modifiers = [];

    protected $soap_headers = [];

    protected $headers = [];

    protected $soap_options = [];

    protected $saveLast = true;

    protected $useResponse = false;

    protected $last_result;

    protected $xml_parser;

    /**
     * @var Log
     */
    protected $logger;

    protected $elapsed = [
        'start'   => 0,
        'client'  => 0,
        'request' => 0,
        'total'   => 0,
    ];

    public function __construct($clientClass)
    {
        $this->clientClass = $clientClass;
    }

    /**
     * @param string $function
     * @param mixed  $request
     * @return $this
     */
    public function asFunction($function, $request = null)
    {
        $this->function = $function;
        if (!is_null($request)) {
            $this->request($request);
        }
        return $this;
    }

    /**
     * @param mixed $request
     * @return $this
     */
    public function request($request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @param string $result
     * @return $this
     */
    public function useLast($result)
    {
        $this->last_result = $result;
        $this->saveLastSoapRequest();
        return $this;
    }

    public function getUseLast()
    {
        return $this->last_result;
    }

    /**
     * Store the last soap request debug info on the model
     * @return $this
     */
    public function saveLastSoapRequest()
    {
        $this->saveLast = true;
        return $this;
    }

    /**
     * @return $this|\Smorken\Soap\Request
     */
    public function saveLast()
    {
        return $this->saveLastSoapRequest();
    }

    /**
     * @return \Smorken\Soap\Contracts\Soap\Model|null
     */
    public function first()
    {
        $results = $this->run();
        if ($results && is_array($results)) {
            $result = array_shift($results);
            if ($result) {
                return $this->getModel()
                            ->newInstance($result);
            }
        }
        return null;
    }

    /**
     * @return array<array>|null
     */
    public function run()
    {
        if ($this->verify()) {
            $this->setElapsed('start');
            list($results, $last) = $this->getResultsAndLast();
            return $this->fromResult($results, $last);

        }
        return null;
    }

    protected function getResultsAndLast()
    {
        $last = $this->getUseLast();
        if ($last && isset($last['response']['parsed'])) {
            $this->setElapsed('client');
            return [unserialize($last['response']['parsed']), $last];
        }
        $client = $this->buildClient();
        $this->setElapsed('client');
        try {
            $results = $client->request($this->getFunction(), $this->getRequest());
        } catch (\SoapFault $e) {
            $this->logFault($e);
            throw $e;
        }
        return [$results, $this->getLastSoapRequestWithParsed($results, $client)];
    }

    protected function getLastSoapRequestWithParsed($results, $client)
    {
        $debug = null;
        if ($this->saveLast) {
            $debug = $client->debugLastSoapRequest();
            $debug['response']['parsed'] = serialize($results);
        }
        return $debug;
    }

    protected function fromResult($results, $last = null)
    {
        $results = $this->ensureResults($results);
        $results = $this->swapResponse($last, $results);
        $this->setElapsed('request');
        $results = $this->parseResultsWithParsers($results);
        $results = $this->parseResultsForKey($results);
        if ($this->saveLast) {
            $this->getModel()
                 ->setLastSoapRequest($last);
        }
        $this->setElapsed('total');
        $this->getModel()
             ->setElapsed($this->elapsed);
        return $results;
    }

    protected function logFault(\SoapFault $e)
    {
        if (property_exists($e, 'detail')) {
            $details = $e->detail;
            $this->getLogger()
                 ->error(json_encode($details));
        }
    }

    protected function getLogger()
    {
        if (!$this->logger) {
            $this->logger = app(Log::class);
        }
        return $this->logger;
    }

    public function setLogger(Log $logger)
    {
        $this->logger = $logger;
        return $this;
    }

    protected function ensureResults($results)
    {
        if (is_null($results) || $results === '') {
            return [];
        }
        return $results;
    }

    protected function ensureArray($results)
    {
        if (array_keys($results) === range(0, count($results) - 1)) {
            return $results;
        }
        return [$results];
    }

    protected function verify()
    {
        if (!$this->getFunction() && !$this->getUseLast()) {
            throw new RequestException('No function provided.');
        }
        if (!$this->getWsdl()) {
            $opts = $this->getSoapOptions();
            if (!isset($opts['location']) && !isset($opts['uri'])) {
                throw new RequestException('No WSDL provided or no non-WSDL options provided.');
            }
        }
        return true;
    }

    /**
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * @return string
     */
    public function getWsdl()
    {
        return $this->wsdl;
    }

    /**
     * @return array
     */
    public function getSoapOptions()
    {
        return $this->soap_options;
    }

    protected function setElapsed($key)
    {
        $this->elapsed[$key] = microtime(true);
    }

    protected function buildClient()
    {
        $cls = $this->clientClass;
        $soap_opts = $this->getSoapOptions();
        if ($this->shouldUseResponse()) {
            $soap_opts['trace'] = true;
        }
        $this->addHttpHeaders($soap_opts);
        $this->addClassMap($soap_opts);
        $client = new $cls($this->getWsdl(), $soap_opts);
        $this->addSoapHeaders($client);
        $this->addModifiers($client);
        return $client;
    }

    public function shouldUseResponse()
    {
        return $this->useResponse;
    }

    protected function addHttpHeaders(&$soap_opts)
    {
        if ($this->getHttpHeaders()) {
            $context = stream_context_create();
            if (!isset($soap_opts['stream_context'])) {
                $soap_opts['stream_context'] = $context;
            } else {
                $context = $soap_opts['stream_context'];
            }
            stream_context_set_option($context, ['http' => ['header' => implode("\r\n", $this->getHttpHeaders())]]);
        }
    }

    /**
     * @return array
     */
    public function getHttpHeaders()
    {
        return $this->headers;
    }

    protected function addClassMap(&$soap_opts)
    {
        if ($this->getClassMap()) {
            $cm = $this->getClassMap();
            $coll = $cm;
            $soap_opts['classmap'] = $coll;
        }
    }

    /**
     * @return mixed
     */
    public function getClassMap()
    {
        return $this->classMap;
    }

    protected function addSoapHeaders(\Smorken\Soap\Contracts\Soap\Client $client)
    {
        if ($this->getSoapHeaders()) {
            $client->applySoapHeaders($this->getSoapHeaders());
        }
    }

    /**
     * @return array
     */
    public function getSoapHeaders()
    {
        return $this->soap_headers;
    }

    protected function addModifiers(\Smorken\Soap\Contracts\Soap\Client $client)
    {
        foreach ($this->getModifiers() as $key => $handler) {
            $client->modifier($key, $handler);
        }
    }

    /**
     * @return array
     */
    public function getModifiers()
    {
        return $this->modifiers;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    protected function swapResponse($last, $results)
    {
        if ($this->shouldUseResponse()) {
            $body = $last['response']['body'];
            if ($body) {
                $parser = $this->getXmlParser();
                return $this->getSoapBody($parser->parse($body));
            }
        }
        return $results;
    }

    protected function getSoapBody($results)
    {
        if (isset($results['Envelope']['Body'])) {
            return $results['Envelope']['Body'];
        }
        if (isset($results['soap:Envelope']['soap:Body'])) {
            return $results['soap:Envelope']['soap:Body'];
        }
        return $results;
    }

    protected function parseResultsWithParsers($results)
    {
        $parsers = $this->getParsers();
        if ($parsers) {
            foreach ($parsers as $parser) {
                $results = $parser->parse($results);
            }
        }
        return $results;
    }

    protected function getParsers()
    {
        if ($this->getModel()) {
            return $this->getModel()
                        ->getParsers();
        }
    }

    /**
     * @return \Smorken\Soap\Contracts\Soap\Model
     * @throws ResponseException
     */
    public function getModel()
    {
        if (!$this->model) {
            throw new \UnexpectedValueException("Model must be set.");
        }
        return $this->model;
    }

    public function setModel(\Smorken\Model\Contracts\Model $model)
    {
        $this->model = $model;
        return $this;
    }

    protected function parseResultsForKey($results)
    {
        if ($this->getModel()
                 ->getResultKey()) {
            return $this->findKeyedResults($results, $this->getModel()
                                                          ->getResultKey()
            );
        }
        return $results;
    }

    protected function findKeyedResults($results, $key)
    {
        $parsed = [];
        if (!$results) {
            return $results;
        }
        if (is_array($results)) {
            if (array_key_exists($key, $results)) {
                $parsed[] = $results[$key];
            } else {
                foreach ($results as $k => $v) {
                    if (is_array($v)) {
                        $search = $this->findKeyedResults($v, $key);
                        if ($search) {
                            return $this->ensureArray($search);
                        }
                    }
                }
            }
        }
        return $parsed;
    }

    public function useResponse()
    {
        $this->useResponse = true;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientClass()
    {
        return $this->clientClass;
    }

    /**
     * @return Collection<\Smorken\Soap\Contracts\Soap\Model>
     */
    public function all()
    {
        $results = $this->run();
        $collection = new Collection();
        if (is_array($results)) {
            foreach ($results as $result) {
                $collection->push(
                    $this->getModel()
                         ->newInstance($result)
                );
            }
        }
        return $collection;
    }

    /**
     * @param                $key
     * @param callable|array $callable
     */
    public function modifier($key, $callable)
    {
        $this->modifiers[$key] = $callable;
        return $this;
    }

    /**
     * @param array $headers
     */
    public function httpHeaders($headers = [])
    {
        foreach ($headers as $header) {
            $this->httpHeader($header);
        }
        return $this;
    }

    /**
     * @param string $header
     */
    public function httpHeader($header)
    {
        $this->headers[] = $header;
        return $this;
    }

    /**
     * @param \SoapHeader $header
     */
    public function soapHeader(\SoapHeader $header)
    {
        $this->soap_headers[] = $header;
        return $this;
    }

    /**
     * @param array $options
     */
    public function soapOptions($options = [])
    {
        foreach ($options as $k => $v) {
            $this->soapOption($k, $v);
        }
        return $this;
    }

    /**
     * @param string $key
     * @param mixed  $value
     */
    public function soapOption($key, $value)
    {
        $this->soap_options[$key] = $value;
        return $this;
    }

    /**
     * @param string $wsdl
     */
    public function wsdl($wsdl)
    {
        $this->wsdl = $wsdl;
        return $this;
    }

    /**
     * @param mixed $classMap
     * @return $this
     */
    public function classMap($classMap)
    {
        $this->classMap = $classMap;
        return $this;
    }

    /**
     * @return \Smorken\Soap\Contracts\Type\XmlToArray
     */
    protected function getXmlParser()
    {
        if (!$this->xml_parser) {
            $this->xml_parser = new XmlToArray();
        }
        return $this->xml_parser;
    }

    /**
     * @param \Smorken\Soap\Contracts\Type\XmlToArray $parser
     */
    public function setXmlParser($parser)
    {
        $this->xml_parser = $parser;
    }
}
