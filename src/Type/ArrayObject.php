<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/28/17
 * Time: 2:14 PM
 */

namespace Smorken\Soap\Type;

use Smorken\Soap\RequestException;

abstract class ArrayObject extends \ArrayObject
{

    protected $name;

    protected $namespace;

    public function __construct($input = [], $flags = 0, $iterator_class = "ArrayIterator")
    {
        foreach($input as $i => $item) {
            $input[$i] = $this->toSoapVar($item);
        }
        parent::__construct($input, $flags, $iterator_class);
    }

    public function append($value)
    {
        parent::append($this->toSoapVar($value));
    }

    protected function getName()
    {
        if (!$this->name) {
            throw new RequestException('Name must be set.');
        }
        return $this->name;
    }

    protected function getNamespace()
    {
        return $this->namespace;
    }

    public function setNamespace($ns)
    {
        $this->namespace = $ns;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    protected function toSoapVar($value)
    {
        return new \SoapVar($value, SOAP_ENC_OBJECT, null, $this->getNamespace(), $this->getName());
    }
}
