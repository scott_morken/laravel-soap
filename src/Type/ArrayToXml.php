<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/28/17
 * Time: 3:26 PM
 */

namespace Smorken\Soap\Type;

class ArrayToXml implements \Smorken\Soap\Contracts\Type\ArrayToXml
{

    public function create($array, $root = 'root')
    {
        $xml = \Spatie\ArrayToXml\ArrayToXml::convert($array, $root);
        return $this->stripXml($xml);
    }

    protected function stripXml($xml)
    {
        return trim(preg_replace('/<\?xml.*\?>/', '', $xml));
    }
}
