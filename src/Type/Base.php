<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/28/17
 * Time: 1:14 PM
 */

namespace Smorken\Soap\Type;

abstract class Base
{

    public function toArray()
    {
        $vars = get_object_vars($this);
        return $this->asArray($vars);
    }

    protected function asArray($vars)
    {
        $arr = [];
        if (!$vars) {
            return null;
        }
        if (is_array($vars)) {
            foreach($vars as $k => $v) {
                if (is_object($v) && method_exists($v, 'toArray')) {
                    $arr[$k] = $v->toArray();
                } elseif (is_array($v)) {
                    $arr[$k] = $this->asArray($v);
                } else {
                    $arr[$k] = $this->getByKey($k) ?: $v;
                }
            }
        }
        return $arr;
    }

    public function getByKey($key)
    {
        $methodname = 'get' . ucfirst($key);
        if (method_exists($this, $methodname)) {
            return $this->$methodname();
        }
        if (property_exists($this, $key)) {
            return $this->$key;
        }
        return null;
    }

    public function __get($key)
    {
        return $this->getByKey($key);
    }

    public function __set($key, $value)
    {
        $methodname = 'set' . ucfirst($key);
        if (method_exists($this, $methodname)) {
            $this->$methodname($value);
        }
        if (property_exists($this, '_') && is_array($this->_)) {
            if (array_key_exists($key, $this->_)) {
                $this->_[$key] = $value;
            }
        }
        $this->$key = $value;
    }
}
