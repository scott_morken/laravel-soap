<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/28/17
 * Time: 6:33 PM
 */

namespace Smorken\Soap\Type;

class XmlToArray implements \Smorken\Soap\Contracts\Type\XmlToArray
{

    // xml parser
    private $parser = null;

    public function __construct()
    {
        $this->parser = xml_parser_create();
    }

    public function parse($xml)
    {
        return $this->xml2array($xml);
    }

    /**
     * xml2array() will convert the given XML text to an array in the XML structure.
     * Link: http://www.bin-co.com/php/scripts/xml2array/
     * Arguments : $contents - The XML text
     * Return: The parsed XML in an array form. Use print_r() to see the resulting array structure.
     * Examples: $array =  xml2array(file_get_contents('feed.xml'));
     *              $array =  xml2array(file_get_contents('feed.xml'));
     * @param $contents
     * @return array
     */
    function xml2array($contents)
    {
        if (!$contents) {
            return [];
        }

        if (!function_exists('xml_parser_create')) {
            //print "'xml_parser_create()' function not found!";
            return [];
        }

        //Get the XML parser of PHP - PHP must have this module for the parser to work
        $parser = $this->parser;
        xml_parser_set_option(
            $parser,
            XML_OPTION_TARGET_ENCODING,
            "UTF-8"
        ); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($contents), $xml_values);
        xml_parser_free($parser);

        if (!$xml_values) {
            return;
        }//Hmm...

        //Initializations
        $xml_array = [];
        $parents = [];
        $opened_tags = [];
        $arr = [];

        $current = &$xml_array; //Reference

        //Go through the tags.
        $repeated_tag_index = [];//Multiple tags with same name will be turned into an array
        foreach ($xml_values as $data) {
            unset($attributes, $value);//Remove existing values, or there will be trouble

            //This command will extract these variables into the foreach scope
            // tag(string), type(string), level(int), attributes(array).
            /**
             * @var string $tag
             * @var string $type
             * @var int $level
             * @var array $attributes
             */
            extract($data);//We could use the array by itself, but this cooler.

            $result = [];
            $tag = $this->stripNs($tag);
            if (isset($value)) {
                $result = $value;
            }

            //Set the attributes too.
            if (isset($attributes)) {
                $this->addAttributes($result, $attributes);
            }

            //See tag status and do the needed.
            if ($type == "open") {//The starting of the tag '<tag>'
                $parent[$level - 1] = &$current;

                if (!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
                    $current[$tag] = $result;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    $current = &$current[$tag];
                } else { //There was another element with the same tag name
                    if (isset($current[$tag][0])) {//If there is a 0th element it is already an array
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        $repeated_tag_index[$tag . '_' . $level]++;
                    } else {//This section will make the value an array if multiple tags with the same name appear together
                        $current[$tag] = [
                            $current[$tag],
                            $result,
                        ];//This will combine the existing item and the new item together to make an array
                        $repeated_tag_index[$tag . '_' . $level] = 2;
                    }
                    $last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
                    $current = &$current[$tag][$last_item_index];
                }
            } elseif ($type == "complete") { //Tags that ends in 1 line '<tag />'
                //See if the key is already taken.
                if (!isset($current[$tag])) { //New Key
                    $current[$tag] = $result;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                } else { //If taken, put all things inside a list(array)
                    if (isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

                        // ...push the new element into that array.
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        $repeated_tag_index[$tag . '_' . $level]++;
                    } else { //If it is not an array...
                        $current[$tag] = [
                            $current[$tag],
                            $result,
                        ]; //...Make it an array using using the existing value and the new value
                        $repeated_tag_index[$tag . '_' . $level] = 1;
                        $repeated_tag_index[$tag . '_' . $level]++; //0 and 1 index is already taken
                    }
                }
            } elseif ($type == 'close') { //End of tag '</tag>'
                $current = &$parent[$level - 1];
            }
        }

        return ($xml_array);
    }

    protected function addAttributes(&$current, $attributes)
    {
        foreach($attributes as $k => $v) {
            $k = '_' . $this->stripNs($k);
            $current[$k] = $v;
        }
    }

    protected function stripNs($item)
    {
        if (($i = strpos($item, ':')) !== false) {
            $item = substr($item, ($i + 1));
        }
        return $item;
    }
}
