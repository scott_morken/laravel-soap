<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/21/17
 * Time: 10:16 AM
 */

namespace Smorken\Soap;

class Client extends \SoapClient implements \Smorken\Soap\Contracts\Soap\Client
{

    use \Smorken\Soap\Traits\Client;

    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        $request = $this->modify('request', $request);
        $location = $this->modify('location', $location);
        $action = $this->modify('action', $action);
        $version = $this->modify('version', $version);
        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }
}
