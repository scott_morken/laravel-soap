<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 2:54 PM
 */

namespace Smorken\Soap;

class RequestException extends \Exception implements \Smorken\Model\Contracts\RequestException
{

}
