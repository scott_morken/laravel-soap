<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/29/17
 * Time: 10:35 AM
 */

namespace Smorken\Soap\Traits;

trait Client
{

    protected $modifiers = [];

    /**
     * @param string $key
     * @param callable|array $handler
     * @return $this
     */
    public function modifier($key, $handler)
    {
        $this->modifiers[$key] = $handler;
        return $this;
    }

    /**
     * @param $func
     * @param mixed $request
     * @return mixed
     */
    public function request($func, $request)
    {
        return call_user_func([$this, $func], $request);
    }

    public function debugLastSoapRequest()
    {
        return [
            'request'  => [
                'headers' => $this->__getLastRequestHeaders(),
                'body'    => $this->__getLastRequest(),
            ],
            'response' => [
                'headers' => $this->__getLastResponseHeaders(),
                'body'    => $this->__getLastResponse(),
            ],
        ];
    }

    public function applySoapHeaders($soapHeaders)
    {
        $this->__setSoapHeaders($soapHeaders);
        return $this;
    }

    protected function modify($key, $value)
    {
        if (isset($this->modifiers[$key])) {
            $handlers = $this->modifiers[$key];
            if (!is_array($handlers)) {
                $handlers = (array)$handlers;
            }
            foreach ($handlers as $handler) {
                if (is_callable($handler)) {
                    $value = $handler($value);
                }
            }
        }
        return $value;
    }
}
