<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/28/17
 * Time: 3:40 PM
 */

namespace Tests\Smorken\Soap\integration\Type;

use PHPUnit\Framework\TestCase;
use Smorken\Soap\Type\ArrayToXml;
use Tests\Smorken\Soap\integration\Soap\Types\Animals;
use Tests\Smorken\Soap\integration\Soap\Types\Dog;

class ArrayToXmlTest extends TestCase
{

    /**
     *
     */
    public function testToXml()
    {
        $dog_arr[] = new Dog('Fido');
        $dog_arr[] = new Dog('Suzy', 'Golden Retriever', 'Female');
        $animals = new Animals($dog_arr);
        $a = $animals->toArray();
        $xml = $this->getSut()->create($a, 'Animals');
        $expected = '<Animals foo="bar"><Dogs><Dog breed="Pooch" gender="Male"><Name>Fido</Name></Dog><Dog breed="Golden Retriever" gender="Female"><Name>Suzy</Name></Dog></Dogs></Animals>';
        $this->assertContains($expected, $xml);
    }

    protected function getSut()
    {
        return new ArrayToXml();
    }
}
