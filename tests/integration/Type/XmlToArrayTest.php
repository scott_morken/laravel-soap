<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/28/17
 * Time: 6:48 PM
 */

namespace Tests\Smorken\Soap\integration\Type;

use PHPUnit\Framework\TestCase;
use Smorken\Soap\Type\XmlToArray;

class XmlToArrayTest extends TestCase
{

    public function testXmlToArray()
    {
        $sut = $this->getSut();
        $xml = $this->getXml();
        $results = $sut->parse($xml);
        $this->assertArrayHasKey('Envelope', $results);
        $this->assertArrayHasKey('_soapenv', $results['Envelope']);
        $this->assertArrayHasKey('Body', $results['Envelope']);
        $this->assertArrayHasKey('SCC_LOV_RESP', $results['Envelope']['Body']);
        $this->assertArrayHasKey('LOVS', $results['Envelope']['Body']['SCC_LOV_RESP']);
        $this->assertArrayHasKey('LOV', $results['Envelope']['Body']['SCC_LOV_RESP']['LOVS']);
        $lovs = $results['Envelope']['Body']['SCC_LOV_RESP']['LOVS']['LOV'];
        $this->assertCount(3, $lovs);
        foreach ($lovs as $lov) {
            $this->assertArrayHasKey('_name', $lov);
            $this->assertArrayHasKey('VALUES', $lov);
            $this->assertArrayHasKey('VALUE', $lov['VALUES']);
            $this->assertArrayHasKey('CODE', $lov['VALUES']['VALUE']);
            $this->assertArrayHasKey('DESC', $lov['VALUES']['VALUE']);
        }
    }

    protected function getSut()
    {
        return new XmlToArray();
    }

    protected function getXml()
    {
        return file_get_contents(__DIR__ . '/../fixtures/lov.xml');
    }
}
