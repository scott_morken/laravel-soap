<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/28/17
 * Time: 1:41 PM
 */

namespace Tests\Smorken\Soap\integration\Soap\Types;

use Phpro\SoapClient\Type\RequestInterface;
use Smorken\Soap\Type\Base;

class Animals extends Base
{

    protected $Dogs;

    protected $_attributes = [
        'foo' => 'bar',
    ];

    public function __construct(array $dogs)
    {
        $this->Dogs['Dog'] = $dogs;
    }
}
