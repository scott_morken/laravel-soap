<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/28/17
 * Time: 1:44 PM
 */

namespace Tests\Smorken\Soap\integration\Soap\Types;

use Smorken\Soap\Type\Base;

class Dog extends Base
{

    protected $_attributes = [
        'breed' => 'Pooch',
        'gender' => 'Male',
    ];

    protected $Name;

    public function __construct($name, $breed = 'Pooch', $gender = 'Male')
    {
        $this->Name = $name;
        $this->_attributes['breed'] = $breed;
        $this->_attributes['gender'] = $gender;
    }
}
