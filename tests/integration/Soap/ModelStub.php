<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/28/17
 * Time: 9:52 AM
 */

namespace Tests\Smorken\Soap\integration\Soap;

use Smorken\Soap\Model;

class ModelStub extends Model implements \Smorken\Soap\Contracts\Soap\Model
{
    protected $parser_class = [];
}
