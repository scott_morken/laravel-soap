<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/28/17
 * Time: 9:44 AM
 */

namespace Tests\Smorken\Soap\integration\Soap;

use PHPUnit\Framework\TestCase;
use Smorken\Soap\Contracts\Soap\Model;
use Smorken\Soap\RequestObject;
use Smorken\Soap\Type\ArrayToXml;
use Tests\Smorken\Soap\integration\Soap\Types\Animals;
use Tests\Smorken\Soap\integration\Soap\Types\Dog;

class ModelTest extends TestCase
{

    public function testSimpleRequest()
    {
        $gclient = $this->getClientClass($this->getResponseXml());
        list($m) = $this->getModel(ModelStub::class);
        $dog_arr[] = new Dog('Fido');
        $dog_arr[] = new Dog('Suzy', 'Golden Retriever', 'Female');
        $animals = new Animals($dog_arr);
        $req = new RequestObject($animals, 'Animals');
        $result = $m->newRequest()->asFunction('SOAP_FUNC', $req->getRequest(true))->run();
        $request = $m->getLastSoapRequest()['request']['body'];
        $xml = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://target.namespace" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><SOAP-ENV:Body><ns1:SOAP_FUNC><Animals foo="bar"><Dogs><Dog breed="Pooch" gender="Male"><Name>Fido</Name></Dog><Dog breed="Golden Retriever" gender="Female"><Name>Suzy</Name></Dog></Dogs></Animals></ns1:SOAP_FUNC></SOAP-ENV:Body></SOAP-ENV:Envelope>';
        $this->assertContains($xml, $request);
    }

    public function testSaveLastAddsParsedResponse()
    {
        $gclient = $this->getClientClass($this->getResponseXml());
        list($m) = $this->getModel(ModelStub::class);
        $dog_arr[] = new Dog('Fido');
        $dog_arr[] = new Dog('Suzy', 'Golden Retriever', 'Female');
        $animals = new Animals($dog_arr);
        $req = new RequestObject($animals, 'Animals');
        $result = $m->newRequest()->asFunction('SOAP_FUNC', $req->getRequest(true))->saveLast()->run();
        $parsed = unserialize($m->getLastSoapRequest()['response']['parsed']);
        $this->assertInstanceOf('stdClass', $parsed);
        $this->assertCount(34, $parsed->HolidayCode);
        $this->assertNotSame($result, $parsed);
    }

    public function testUseXmlResponse()
    {
        $gclient = $this->getClientClass($this->getResponseXml());
        list($m) = $this->getModel(ModelStub::class);
        $dog_arr[] = new Dog('Fido');
        $dog_arr[] = new Dog('Suzy', 'Golden Retriever', 'Female');
        $animals = new Animals($dog_arr);
        $req = new RequestObject($animals, 'Animals');
        $result = $m->newRequest()->asFunction('SOAP_FUNC', $req->getRequest(true))->useResponse()->run();
        $request = $m->getLastSoapRequest()['request']['body'];
        $xml = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://target.namespace" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><SOAP-ENV:Body><ns1:SOAP_FUNC><Animals foo="bar"><Dogs><Dog breed="Pooch" gender="Male"><Name>Fido</Name></Dog><Dog breed="Golden Retriever" gender="Female"><Name>Suzy</Name></Dog></Dogs></Animals></ns1:SOAP_FUNC></SOAP-ENV:Body></SOAP-ENV:Envelope>';
        $this->assertContains($xml, $request);
    }

    protected function prettyString($xml)
    {
        $doc = new \DOMDocument();
        $doc->loadXML($xml);
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;
        $str = $doc->saveXML();
        return $str;
    }
    protected function getClientClass($response = null)
    {
        if (!is_null($response)) {
            ClientStub::setResponse($response);
        }
        return ClientStub::class;
    }

    protected function getResponseXml()
    {
        return file_get_contents(__DIR__ . '/../fixtures/test_response.xml');
    }

    protected function getModel($model_cls)
    {
        /**
         * @var Model $m
         */
        $m = new $model_cls;
        $model_cls::setClientClass(ClientStub::class);
        $m->setWsdl(null);
        $opts = [
            'location' => 'https://example.org',
            'uri'      => 'http://target.namespace',
            'trace'    => 1,
        ];
        $m->setClientOptions(['foo']);
        $m->setSoapOptions($opts);
        $model_cls::setClientClass($this->getClientClass());
        return [$m];
    }
}
