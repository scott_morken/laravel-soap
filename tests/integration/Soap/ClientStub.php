<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/28/17
 * Time: 9:46 AM
 */

namespace Tests\Smorken\Soap\integration\Soap;

use Smorken\Soap\Traits\Client;

class ClientStub extends SoapClientStub implements \Smorken\Soap\Contracts\Soap\Client
{

    use Client;

    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        $request = $this->modify('request', $request);
        $location = $this->modify('location', $location);
        $action = $this->modify('action', $action);
        $version = $this->modify('version', $version);
        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }
}
