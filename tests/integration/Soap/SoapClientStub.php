<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/29/17
 * Time: 10:33 AM
 */

namespace Tests\Smorken\Soap\integration\Soap;

class SoapClientStub extends \SoapClient
{

    protected static $response = '';

    public static function setResponse($response)
    {
        static::$response = $response;
    }

    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        return static::$response;
    }
}
