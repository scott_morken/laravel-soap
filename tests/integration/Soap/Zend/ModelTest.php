<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/29/17
 * Time: 9:21 AM
 */

namespace Tests\Smorken\Soap\integration\Soap\Zend;

use Illuminate\Contracts\Config\Repository;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Soap\Parsers\LovWithAttributes;
use Smorken\Soap\Parsers\LovWithValues;
use Smorken\Soap\Parsers\QueryServiceResults;
use Smorken\Soap\Zend\Model;
use Tests\Smorken\Soap\integration\Soap\SoapClientStub;

class ModelTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testGetResponse()
    {
        SoapClientStub::setResponse($this->getResponseXml());
        list($m) = $this->getModel(ModelStub::class);
        $response = $m->newRequest()->asFunction('Foo', [])->useResponse()->run();
        $this->assertArrayHasKey('SCC_LOV_RESP', $response);
        $this->assertArrayHasKey('LOVS', $response['SCC_LOV_RESP']);
        $this->assertArrayHasKey('LOV', $response['SCC_LOV_RESP']['LOVS']);
        $lovs = $response['SCC_LOV_RESP']['LOVS']['LOV'];
        $this->assertCount(3, $lovs);
        $this->assertEquals('AID_YEAR', $lovs[0]['_name']);
    }

    protected function getResponseXml($name = 'lov')
    {
        return file_get_contents(sprintf('%s/../../fixtures/%s.xml', __DIR__, $name));
    }

    protected function getModel($model_cls)
    {
        /**
         * @var Model $m
         */
        $m = new $model_cls;
        $model_cls::setClientClass(ClientStub::class);
        $m->setWsdl(null);
        $opts = [
            'location' => 'https://example.org',
            'uri'      => 'http://target.namespace',
            'trace'    => 1,
        ];
        $m->setSoapOptions($opts);
        $this->addConfigToModel($m);
        return [$m];
    }

    protected function addConfigToModel($m)
    {
        $c = m::mock(Repository::class);
        $c->shouldReceive('get')->with('soap.client_options', [])->andReturn([]);
        $c->shouldReceive('get')->with('soap.soap_options', [])->andReturn([]);
        $m->setConfig($c);
    }

    public function testGetResponseWithLovParser()
    {
        SoapClientStub::setResponse($this->getResponseXml());
        list($m) = $this->getModel(ModelStub::class);
        $m->setParserClass(LovWithValues::class);
        //$m->setResultKey('SCC_LOV_RESP');
        $response = $m->newRequest()->asFunction('Foo', [])->useResponse()->run();
        $this->assertCount(1, $response);
        $this->assertEquals('2017', $response[0]['AID_YEAR']);
        $this->assertEquals('2016-08-20', $response[0]['ACAD_YEAR_START']);
    }

    public function testGetResponseWithLovAttributeParser()
    {
        SoapClientStub::setResponse($this->getResponseXml('lov_attrs'));
        list($m) = $this->getModel(ModelStub::class);
        $m->setParserClass(LovWithAttributes::class);
        //$m->setResultKey('SCC_LOV_RESP');
        $response = $m->newRequest()->asFunction('Foo', [])->useResponse()->run();
        $this->assertCount(3, $response);
        $this->assertEquals('2018', $response[0]['AID_YEAR']);
        $this->assertEquals('COLL01', $response[0]['INSTITUTION']);
        $this->assertEquals('2017', $response[2]['AID_YEAR']);
        $this->assertEquals('COLL01', $response[2]['INSTITUTION']);
    }


    public function testGetResponseWithQueryServiceParser()
    {
        SoapClientStub::setResponse($this->getResponseXml('queryservice'));
        list($m) = $this->getModel(ModelStub::class);
        $m->setParserClass(QueryServiceResults::class);
        //$m->setResultKey('SCC_LOV_RESP');
        $response = $m->newRequest()->asFunction('Foo', [])->useResponse()->run();
        $this->assertCount(5, $response);
        $expected = [
            'EMPLID'            => '30000000',
            'INSTITUTION'       => 'COLL03',
            'ACAD_CAREER'       => 'CRED',
            'STRM'              => '2222',
            'DESCR'             => 'Fall 2017',
            'SFA_SAP_STAT_CALC' => 'NMET',
            'SFA_SAP_STATUS'    => [],
            'PROCESS_DTTM'      => '2017-05-10T15:04:24-0600',
            '_rownumber'        => '1',
        ];
        $this->assertEquals($expected, $response[0]);
    }
}
