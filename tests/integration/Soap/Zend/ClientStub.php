<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/29/17
 * Time: 9:21 AM
 */

namespace Tests\Smorken\Soap\integration\Soap\Zend;

use Smorken\Soap\Zend\Client;
use Tests\Smorken\Soap\integration\Soap\SoapClientStub;
use Zend\Soap\Client\Common;
use Zend\Soap\Exception\ExceptionInterface;
use Zend\Soap\Exception\UnexpectedValueException;

class ClientStub extends Client implements \Smorken\Soap\Contracts\Soap\Client
{

    /**
     * Initialize SOAP Client object
     *
     * @throws ExceptionInterface
     */
    protected function initSoapClientObject()
    {
        $wsdl = $this->getWSDL();
        $options = array_merge($this->getOptions(), ['trace' => true]);
        if ($wsdl === null) {
            if (!isset($options['location'])) {
                throw new UnexpectedValueException('"location" parameter is required in non-WSDL mode.');
            }
            if (!isset($options['uri'])) {
                throw new UnexpectedValueException('"uri" parameter is required in non-WSDL mode.');
            }
        } else {
            if (isset($options['use'])) {
                throw new UnexpectedValueException('"use" parameter only works in non-WSDL mode.');
            }
            if (isset($options['style'])) {
                throw new UnexpectedValueException('"style" parameter only works in non-WSDL mode.');
            }
        }
        unset($options['wsdl']);
        $this->soapClient = new SoapClientStub($wsdl, $options);
    }
}
