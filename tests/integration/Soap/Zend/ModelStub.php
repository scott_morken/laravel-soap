<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/29/17
 * Time: 9:35 AM
 */

namespace Tests\Smorken\Soap\integration\Soap\Zend;

use Smorken\Soap\Zend\Model;

class ModelStub extends Model implements \Smorken\Soap\Contracts\Soap\Model
{

    protected $request_class = RequestStub::class;

    /**
     * Get the client class for the model.
     *
     * @return string
     */
    public function getClientClass()
    {
        if (!static::$clientClass) {
            $cls = ClientStub::class;
            static::setClientClass($cls);
        }
        return static::$clientClass;
    }
}
