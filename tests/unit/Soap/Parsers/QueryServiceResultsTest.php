<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/27/17
 * Time: 9:30 AM
 */

namespace Tests\Smorken\Soap\unit\Soap\Parsers;

use PHPUnit\Framework\TestCase;
use Smorken\Soap\Parsers\QueryServiceResults;

class QueryServiceResultsTest extends TestCase
{

    public function testSimple()
    {
        $sut = $this->getSut();
        $results = $sut->parse($this->getArrayResult());
        $this->assertCount(5, $results);
        $expected = [
            'EMPLID'            => 30000000,
            'INSTITUTION'       => 'COLL03',
            'ACAD_CAREER'       => 'CRED',
            'STRM'              => 2222,
            'DESCR'             => 'Fall 2017',
            'SFA_SAP_STAT_CALC' => 'NMET',
            'SFA_SAP_STATUS'    => '',
            'PROCESS_DTTM'      => '2017-05-10T15:04:24-0600',
        ];
        $this->assertEquals($expected, $results[0]);
    }

    public function testEmpty()
    {
        $sut = $this->getSut();
        $results = $sut->parse($this->getEmptyResult());
        $this->assertCount(0, $results);
    }

    protected function getSut()
    {
        return new QueryServiceResults();
    }

    protected function getEmptyResult()
    {
        return [
            'query' =>
                [
                    'numrows'   => 0,
                    'queryname' => 'SAP_STATUS',
                ],

        ];
    }

    protected function getArrayResult()
    {
        return
            [
                'query' =>
                    [
                        'row'       =>
                            [
                                0 =>
                                    [
                                        'any'       => '<EMPLID><![CDATA[30000000]]></EMPLID><INSTITUTION><![CDATA[COLL03]]></INSTITUTION><ACAD_CAREER><![CDATA[CRED]]></ACAD_CAREER><STRM><![CDATA[2222]]></STRM><DESCR><![CDATA[Fall 2017]]></DESCR><SFA_SAP_STAT_CALC><![CDATA[NMET]]></SFA_SAP_STAT_CALC><SFA_SAP_STATUS/><PROCESS_DTTM>2017-05-10T15:04:24-0600</PROCESS_DTTM>',
                                        'rownumber' => 1,
                                    ],

                                1 =>
                                    [
                                        'any'       => '<EMPLID><![CDATA[30000000]]></EMPLID><INSTITUTION><![CDATA[COLL04]]></INSTITUTION><ACAD_CAREER><![CDATA[CRED]]></ACAD_CAREER><STRM><![CDATA[2222]]></STRM><DESCR><![CDATA[Fall 2017]]></DESCR><SFA_SAP_STAT_CALC><![CDATA[MEET]]></SFA_SAP_STAT_CALC><SFA_SAP_STATUS/><PROCESS_DTTM>2017-05-10T15:24:08-0600</PROCESS_DTTM>',
                                        'rownumber' => 2,
                                    ],

                                2 =>
                                    [
                                        'any'       => '<EMPLID><![CDATA[30000000]]></EMPLID><INSTITUTION><![CDATA[COLL01]]></INSTITUTION><ACAD_CAREER><![CDATA[CRED]]></ACAD_CAREER><STRM><![CDATA[2222]]></STRM><DESCR><![CDATA[Fall 2017]]></DESCR><SFA_SAP_STAT_CALC><![CDATA[MEET]]></SFA_SAP_STAT_CALC><SFA_SAP_STATUS/><PROCESS_DTTM>2017-05-11T09:20:11-0600</PROCESS_DTTM>',
                                        'rownumber' => 3,
                                    ],

                                3 =>
                                    [
                                        'any'       => '<EMPLID><![CDATA[30000000]]></EMPLID><INSTITUTION><![CDATA[COLL09]]></INSTITUTION><ACAD_CAREER><![CDATA[CRED]]></ACAD_CAREER><STRM><![CDATA[2222]]></STRM><DESCR><![CDATA[Fall 2017]]></DESCR><SFA_SAP_STAT_CALC><![CDATA[MEET]]></SFA_SAP_STAT_CALC><SFA_SAP_STATUS/><PROCESS_DTTM>2017-05-11T08:26:36-0600</PROCESS_DTTM>',
                                        'rownumber' => 4,
                                    ],

                                4 =>
                                    [
                                        'any'       => '<EMPLID><![CDATA[30000000]]></EMPLID><INSTITUTION><![CDATA[COLL05]]></INSTITUTION><ACAD_CAREER><![CDATA[CRED]]></ACAD_CAREER><STRM><![CDATA[1111]]></STRM><DESCR><![CDATA[Spring 2016]]></DESCR><SFA_SAP_STAT_CALC><![CDATA[MEET]]></SFA_SAP_STAT_CALC><SFA_SAP_STATUS/><PROCESS_DTTM>2015-12-21T20:31:46-0700</PROCESS_DTTM>',
                                        'rownumber' => 5,
                                    ],

                            ],
                        'numrows'   => 5,
                        'queryname' => 'SAP_STATUS',
                    ],

            ];
    }
}
