<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/19/17
 * Time: 8:37 AM
 */

namespace Tests\Smorken\Soap\unit\Soap\Parsers;

use PHPUnit\Framework\TestCase;
use Smorken\Soap\Parsers\LovWithValues;

class LovWithValuesTest extends TestCase
{

    public function testSimple()
    {
        $sut = $this->getSut();
        $results = $sut->parse($this->getLovsSimple());
        $expected = [
            ['foo_column' => 'foo value 1', 'bar_column' => 'bar value 1'],
        ];
        $this->assertEquals($expected, $results);
    }

    public function testOnce()
    {
        $sut = $this->getSut();
        $results = $sut->parse($this->getLovsOnce());
        $expected = [
            ['foo_column' => 'foo value 1'],
        ];
        $this->assertEquals($expected, $results);
    }

    public function testMultiple()
    {
        $sut = $this->getSut();
        $results = $sut->parse($this->getLovsMultipleValues());
        $expected = [
            ['foo_column' => 'foo value 1', 'bar_column' => 'bar value 1'],
            ['foo_column' => 'foo value 2', 'bar_column' => 'bar value 2'],
        ];
        $this->assertEquals($expected, $results);
    }

    protected function getSut()
    {
        return new LovWithValues();
    }

    protected function getLovsOnce()
    {
        return [
            'LOVS' => [ //can occur once
                        'LOV' => [ //can occur one or more times
                                   'VALUES' => [ //can occur once
                                                 'VALUE' => [ //can occur one or more times
                                                              'CODE' => 'foo value 1',
                                                              'DESC' => 'null',
                                                 ],
                                   ],
                                   '_name'  => 'foo_column',
                        ],
            ],
        ];
    }

    protected function getLovsSimple()
    {
        return [
            'LOVS' => [ //can occur once
                        'LOV' => [ //can occur one or more times
                                   [
                                       'VALUES' => [ //can occur once
                                                     'VALUE' => [ //can occur one or more times
                                                                  'CODE' => 'foo value 1',
                                                                  'DESC' => 'null',
                                                     ],
                                       ],
                                       '_name'  => 'foo_column',
                                   ],
                                   [
                                       'VALUES' => [
                                           'VALUE' => [
                                               'CODE' => 'bar value 1',
                                               'DESC' => 'null',
                                           ],
                                       ],
                                       '_name'  => 'bar_column',
                                   ],
                        ],
            ],
        ];
    }

    protected function getLovsMultipleValues()
    {
        return [
            'LOVS' => [ //can occur once
                        'LOV' => [ //can occur one or more times
                                   [
                                       'VALUES' => [ //can occur once
                                                     [
                                                         'VALUE' => [ //can occur one or more times
                                                                      'CODE' => 'foo value 1',
                                                                      'DESC' => 'null',
                                                         ],
                                                     ],
                                                     [
                                                         'VALUE' => [ //can occur one or more times
                                                                      'CODE' => 'foo value 2',
                                                                      'DESC' => 'null',
                                                         ],
                                                     ],
                                       ],
                                       '_name'  => 'foo_column',
                                   ],
                                   [
                                       'VALUES' => [
                                           [
                                               'VALUE' => [ //can occur one or more times
                                                            'CODE' => 'bar value 1',
                                                            'DESC' => 'null',
                                               ],
                                           ],
                                           [
                                               'VALUE' => [ //can occur one or more times
                                                            'CODE' => 'bar value 2',
                                                            'DESC' => 'null',
                                               ],
                                           ],
                                       ],
                                       '_name'  => 'bar_column',
                                   ],
                        ],
            ],
        ];
    }

    protected function getLovWithAttributes()
    {
        return [
            'LOV' => [
                'VALUES' => [
                    [
                        'foo' => 'bar1',
                        'fiz' => 'buz1',
                    ],
                    [
                        'foo' => 'bar2',
                        'fiz' => 'buz2',
                    ],
                ],
            ],
        ];
    }
}
