<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 3:25 PM
 */
return [
    'soap' => [
        'soap_options'   => [
            // PHP 7+ will currently seg fault if this is not DISK or NONE - 2017-06-15
            'cache_wsdl' => WSDL_CACHE_DISK,
        ],
        'client_options' => [

        ],
    ],
];
